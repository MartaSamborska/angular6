import { Component, OnInit } from '@angular/core';
import { User } from '../users';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
import { UserService } from '../user.service';
import { StorageService } from '../storage-service';

@Component({
  selector: 'app-user-add',
  templateUrl: './user-add.component.html',
  styleUrls: ['./user-add.component.css']
})
export class UserAddComponent implements OnInit {

  private errors: String[]

  constructor(
    private userService: UserService, 
    private location: Location,
    private router: Router
  ) { }

  ngOnInit() {
    this.errors = [];
    if (StorageService.read('access_token') == null) {
      this.router.navigateByUrl('/login');
    }
  }

  create(name: string, lastname: string, email: string, password: string, passwordConfirm: string, typee: number): void {
    if (password === passwordConfirm) {
      this.userService.createUser({name: name, lastname: lastname, email: email, password: password, type: typee} as User)
      .subscribe(() =>  this.router.navigateByUrl('/users'), (error) => {
          this.errors = [];
          this.errors.push("Nie udało się dodać użytkownika");
          console.log(error);
      });
    } else {
      this.errors.push("Hasła muszą być takie same");
    }
  }

  goBack(): void { 
    this.location.back();
  }
}
