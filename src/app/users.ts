export class User {
 id: number;
 type: number;
 name: string;
 lastname: string;
 email: string;
 password: string;
 remember_token: string;
 created_at: string;
 updated_at: string;
 }
