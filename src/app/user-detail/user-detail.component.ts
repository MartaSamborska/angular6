import { Component, OnInit } from '@angular/core';
import { User } from '../user';
import { ActivatedRoute, Router } from '@angular/router';
import { Location } from '@angular/common';
import { UserService } from '../user.service';
import { StorageService } from '../storage-service';

@Component({
  selector: 'app-user-detail',
  templateUrl: './user-detail.component.html',
  styleUrls: ['./user-detail.component.css']
})
export class UserDetailComponent implements OnInit {

  user: User;

  private errors: String[];

  constructor(
    private route: ActivatedRoute,
    private userService: UserService,
    private location: Location,
    private router: Router 
  ) { }

  ngOnInit() {
    if (StorageService.read('access_token') == null) {
      this.router.navigateByUrl('/login');
    } else {
      this.getUser();
    }
    this.errors = [];
  }

  getUser(): void {
    const id = +this.route.snapshot.paramMap.get('id');
    this.userService.getUser(id)
    .subscribe(user => this.user = user, (error) => {
      this.errors = [];
      this.errors.push("Użytkownik nie został znaleziony");
    });
  }

  goBack(): void { 
    this.location.back();
  }

  save(): void {
    this.userService.updateUser(this.user)
    .subscribe(() => this.goBack(), (error) => {
      this.errors = [];
      this.errors.push("Nie udało się zaktualizować danych");
    });
  }

  changePassword(password, passwordConfirm): void {
    if (password === passwordConfirm) {
      this.userService.updatePassword(this.user.id, password)
      .subscribe(() => this.goBack(), (error) => {
        this.errors = [];
        this.errors.push("Nie udało się zmienić hasła");
      });
    } else {
      this.errors.push("Hasła muszą być takie same");
    }
  }
}
