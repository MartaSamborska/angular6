import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';

import { StorageService } from './storage-service';
import { AuthService } from './auth.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit  {

  loggedIn: Boolean
  activeUser: User

  constructor(private location: Location, private authService: AuthService) { 
    this.authService.loginChanged$.subscribe((d) => this.refreshLogin());
  } 

  ngOnInit() {
    this.refreshLogin();
  }

  refreshLogin(): void {
    if (StorageService.read('access_token') == null) {
      this.loggedIn = false;
    } else {
      this.loggedIn = true;
    }
    this.activeUser = StorageService.read('user');
  }

  getClass(path: string): string {
    return (this.location.path() === path) ? 'nav-item active' : 'nav-item';
  }
  
}
