import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { of } from 'rxjs';

import { User } from 'users';
import { StorageService } from 'storage-service';

const httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json', 'Accept': 'application/json, text/javascript' })
};
@Injectable({
  providedIn: 'root'
})
export class UserService {

  private token: string;
  private usersApiUrl = 'http://localhost:8000/api/users';

  constructor(private http: HttpClient) {
  }

  getUsers(): Observable<User[]> {
    return this.http.get<User[]>(this.usersApiUrl);
  }

  getUser(id: number): Observable<User> {
      const url = `${this.usersApiUrl}/${id}`;
      return this.http.get<User>(url);
  }

  updateUser(user: User): Observable<any> {
      const url = `${this.usersApiUrl}/${user.id}`;
      return this.http.put(url, user, httpOptions);
  }

  createUser(user: User): Observable<User> {
      console.log(user);
      return this.http.post<User>(this.usersApiUrl, user, httpOptions);
  }

  deleteUser(user: User | number): Observable<User> {
      const id = typeof user === 'number' ? user : user.id;
      const url = `${this.usersApiUrl}/${id}`;
      return this.http.delete<User>(url, httpOptions);
  }

  updatePassword(user: User | number, password: String): Observable<any> {
      const id = typeof user === 'number' ? user : user.id;
      const url = `${this.usersApiUrl}/changepassword/${id}`;
      return this.http.patch<User>(url, {password: password}, httpOptions);
  }

}
