import { Component, OnInit } from '@angular/core';
import { User } from '../users';
import { Router } from '@angular/router';
import { UserService } from '../user.service';
import { StorageService } from '../storage-service';

@Component({
    selector: 'app-users',
    templateUrl: './users.component.html',
    styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {

    errors: String[]
    activeUser: User
    users: User[]

    constructor(
      private userService: UserService,
      private router: Router,
    ) { }

    ngOnInit() {
      let token = StorageService.read('access_token');
      if (token == null) {
        this.router.navigateByUrl('/login');
      } else {
        this.getUsers();
      }
      this.activeUser = StorageService.read('user'); 
      this.errors = [];
    }

    getUsers(): void {
      this.errors = []; 
      this.userService.getUsers()
      .subscribe(users => this.users = users, errors => this.addError('Nie udało się pobrać użytkowników'));
    }

    delete(user: User): void {
      this.errors = []; 
      if (user.id != this.activeUser.id) {
          this.users = this.users.filter(u => u.id !== user.id);
          this.userService.deleteUser(user.id).subscribe(r => {
              if (r.status == 'ok') {
                  var index = this.users.indexOf(user, 0);
                  if (index > -1) {
                     this.users.splice(index, 1);
                  }
              } else {
                this.addError("Nie udało się usunąć użytkownika");
              }
           }, (error) => {
              this.addError("Nie udało się usunąć użytkownika");
           });
      } else {
         this.addError('Nie możesz usunąć swojego konta');
      }
    }  

    addError(error: String): void {
        this.errors = [];
        this.errors.push(error);
    }

    searchLastname(key: string): void {
        if (key == '') {
            this.getUsers();
        }
        this.users = this.users.filter(u => u.lastname.indexOf(key) > -1 || u.name.indexOf(key) > -1 || u.email.indexOf(key) > -1);
    }
        
}
