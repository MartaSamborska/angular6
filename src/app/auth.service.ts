import { Injectable, EventEmitter } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { of } from 'rxjs';

import { User } from 'users';
import { Credentials } from 'credentials';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json', 'Accept': 'application/json, text/javascript' })
};
@Injectable({
  providedIn: 'root'
})
export class AuthService {

  public loginChanged$: EventEmitter<Boolean>;
  private apiUrl = 'http://localhost:8000/api';

  constructor(private http: HttpClient) {
     this.loginChanged$ = new EventEmitter();
  }

  login(credentials: Credentials): Observable<String> {
      const url = `${this.apiUrl}/login`;
      return this.http.post<String>(url, credentials, httpOptions);
  }
  successLogin() {
    // trigger login event
    this.loginChanged$.emit(true);
  }
  logout() {
    // trigger logout event
    this.loginChanged$.emit(false);
  }
}
