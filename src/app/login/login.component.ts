import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../auth.service';

import { User } from '../user';
import { Credentials } from '../credentials';
import { StorageService } from '../storage-service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loggedIn: Boolean
  private errors: String[]

  constructor(
    private authService: AuthService, 
    private router: Router
  ) { }

  ngOnInit() {
    this.errors = [];
    if (StorageService.read('access_token') == null) {
      this.loggedIn = false;
    } else {
      this.loggedIn = true;
    }
  }

  login(email: string, password: string): void {
    this.authService.login({email: email, password: password} as Credentials)
    .subscribe((r) =>  {
      StorageService.write('access_token', r.token); 
      StorageService.write('user', r.user as User); 
      this.authService.successLogin();
      this.router.navigateByUrl('/')
    }, error => {
      this.errors = [];
      this.errors.push('Niewłaściwe dane logowania');
      console.log('a');
    });
  }

}
