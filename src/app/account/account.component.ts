import { Component, OnInit } from '@angular/core';
import { UserService } from '../user.service';
import { StorageService } from '../storage-service';
import { User } from 'users';

@Component({
  selector: 'app-account',
  templateUrl: './account.component.html',
  styleUrls: ['./account.component.css']
})
export class AccountComponent implements OnInit {

  private errors: String[]
  private info: String[]
  me: User

  constructor(private userService: UserService) { }

  ngOnInit() {
    this.me = StorageService.read('user');
    this.errors = [];
    this.info = [];
  }

  changePassword(password, passwordConfirm): void {
    if (password === passwordConfirm) {
      this.userService.updatePassword(this.me.id, password)
      .subscribe(() => {
        this.errors = [];
        this.info = [];
        this.info.push("Hasło zostało zmienione")
        }, (error) => {
          this.errors = [];
          this.errors.push("Hasło nie zostało zmienione");
        });
    } else {
      this.errors = [];
      this.info = [];
      this.errors.push("Hasła muszą być takie same");
    }
  }

}
