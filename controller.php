<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use App\User;
use Illuminate\Foundation\Auth\ResetsPasswords;

class UserController extends Controller
{
	use ResetsPasswords; // trait to reset user password

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::all();
    	return response()->json($users, 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    	$this->validate($request, [
    		'name' => 'required',
    		'lastname' => 'required',
    		'email' => 'required|email|unique:users,email',
    		'password' => 'required',
    	], [
    		'name.required' => 'Imie jest wymagane',
    		'lastname.required' => 'Nazwisko jest wymagane',
    		'email.required' => 'Adres email jest wymagany',
    		'email.unique' => 'Z tym adresem email jest związane inne konto. Użyj innego adresu.',
    		'email.email' => 'Adres email jest niepoprawny'
    	]);

        try { 
    	$user = User::create([
    			'name' => $request->get('name'),
    			'lastname' => $request->get('lastname'),
    			'email' => $request->get('email'),
    			'password' => bcrypt($request->get('password')),
    	]);
        } catch (\Exception $e) {
    		return response()->json(['error' => 'Nie udało się utworzyć użytkownika'], 400);
        }
        return response()->json($user, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::find($id);
        if ($user) {
    	    return response()->json($user, 200);
        } else {
    	    return response()->json(['error' => 'Nie udało się odnaleść użytkownika'], 404);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
    	$this->validate($request, [
    		'name' => 'required',
    		'lastname' => 'required',
    		'email' => 'required|unique:users,email,'.$id
    	], [
    		'name.required' => 'Imie jest wymagane',
    		'lastname.required' => 'Nazwisko jest wymagane',
    		'email.required' => 'Adres email jest wymagany',
    		'email.unique' => 'Z tym adresem email jest związane inne konto. Użyj innego adresu.'
    	]);

    	$user = User::find($id);
        if ($user) {
            $user->update($request->all()); 
            return response()->json(['status' => 'ok'], 200);
        } else {
    	    return response()->json(['error' => 'Nie udało się odnaleść użytkownika'], 404);
        }
    }

    /**
     * Update password in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function reset(Request $request, $id)
    {
    	$this->validate($request, [
    			'password' => 'required',
    	], [
    			'password.required' => 'Hasło jest wymagane',
    	]);
    	
    	$user = User::find($id);
        if ($user) {
    	    $this->resetPassword($user, $request->get('password'));
            return response()->json(['status' => 'ok'], 200);
        } else {
    	    return response()->json(['error' => 'Nie udało się odnaleść użytkownika'], 404);
        }
    	
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
    	$user = User::find($id);
        if ($user) {
            $user->delete();
            return response()->json(['status' => 'ok'], 200);
        } else {
            return response()->json(['status' => 'ok'], 204);
        }
    }
    
    /**
     * Login user by credentials and return token.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function login(Request $request)
    {
    	// grab credentials from the request
    	$credentials = $request->only(['email', 'password']);
    	
    	try {
    		// attempt to verify the credentials and create a token for the user
    		if (! $token = JWTAuth::attempt($credentials)) {
    			return response()->json(['error' => 'invalid_credentials'], 401);
    		}
    	} catch (JWTException $e) {
    		// something went wrong whilst attempting to encode the token
    		return response()->json(['error' => 'could_not_create_token'], 500);
    	}

    	// all good so return the token
    	return response()->json(['token' => $token, 'user' => JWTAuth::user()]);

    }
    
    /**
     * Get authenticated user.
     *
     * @return \App\User
     */
    public function getAuthenticatedUser()
    {
    	// the token is valid and we have found the user via the sub claim
    	return JWTAuth::parseToken()->authenticate();
    }
}
