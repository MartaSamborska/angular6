<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::post('/login/', 'Api\UserController@login');
Route::resource('users','Api\UserController', [
    'except' => ['create', 'edit']
]);
Route::patch('/users/changepassword/{id}', 'Api\UserController@reset');

Route::middleware('auth:api')->group(function () {
    Route::resource('configurations','Api\ConfigurationController', [
        'only' => ['show']
    ]);
    Route::get('/configurations/{configuration}/html', 'Api\ConfigurationController@showHtml')->name('configurations.html');
});
