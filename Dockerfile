FROM node:10-alpine

ENV HOME=/home/app
ENV APP_NAME=footers

COPY package.json $HOME/$APP_NAME/
#RUN chown -R app:app $HOME/*

#USER app
WORKDIR $HOME/$APP_NAME

RUN npm install -g @angular/cli &&\
    npm install &&\
	npm cache clean --force

CMD ["ng", "serve", "--host", "0.0.0.0"]
